# Compte rendu projet

## 3. Enregistrement du système de fichiers

    /* The pnlfs_type argument is a structure which is set up as follows: */
    
    static struct file_system_type pnlfs_type = {
      .owner    = THIS_MODULE,
      .name   = "pnlfs",
    };
      
    static int __init pnlfs_init(void)
    {
      int e;
      pr_warn("%s : registering start\n", __func__);
      e = register_filesystem(&pnlfs_type);
      if(e){
        pr_warn("%s : registering problem\n", __func__);
        return e;
      }
      pr_warn("%s : registering done\n", __func__);
    return 0;
    }

    static void __exit pnlfs_exit(void)
    {
      pr_warn("%s : start\n", __func__);
      unregister_filesystem(&pnlfs_type);
      pr_warn("%s : end \n", __func__);
    }
      
    module_init(pnlfs_init);
    module_exit(pnlfs_exit);

Note : Ca marche :

    cat /proc/filesystems | grep pnl
    > nodev pnlfs

## 4. Montage d’une partition

### Fonction mount() 
La fonction mount() de la struct file_system_type est appelée pendant l’appel système sys mount. Le noyau fourni des fonctions simplifiant le montage de périphériques selon leur type ( mount_bdev() par exemple). Ces fonctions prennent en paramètre un callback que nous compléterons plus tard (vide pour le moment, retournant -1).

    int fake_callback(struct super_block * sb, void * d, int f)
    {
      pr_warn("%s : start \n", __func__);
      return -1;
      pr_warn("%s : end \n", __func__);
    }
      
    struct dentry * pnlfs_mount
    (struct file_system_type * fst, int f, char const * s , void * d)
    {
      struct dentry * de;
      pr_warn("%s : start \n", __func__);
      de = mount_bdev(fst, f, s, d, fake_callback);
      if (IS_ERR(de))
        pr_err("pnlfs mounting failed\n");
      else
        pr_warn("%s : mount\n", __func__);
      pr_warn("%s : end \n", __func__);
      return de;
    }
      
    static struct file_system_type pnlfs_type = {
      .mount    = pnlfs_mount,
    };


### Fonction kill sb()
Cette fonction est appelée lors de la destruction d’une partition (appel système sys umount). Elle doit appeler la fonction kill_block_super() sur le superbloc en paramètre.

    void pnlfs_unmount (struct super_block * sb)
    {
      pr_warn("%s : start \n", __func__);
      kill_block_super(sb);
      pr_warn("%s : end \n", __func__);
    }
     
    static struct file_system_type pnlfs_type = {
      .kill_sb  = pnlfs_unmount,
    };

Pour monter une image test.img dans le dossier dir/, utiliser la commande :

    mount -t <fsname> -o loop test.img dir/

Note : l’option -o loop permet de monter le fichier test.img sur un des périphériques /dev/loop*

Vérifier que le montage d’une partition échoue (puisque votre callback renvoie -1) et que toutes vos fonctions sont appelées (noter l’ordre d’appel).

Les fonctions sont appelées dans cet ordre :
La fonction pnlfs_mount est appelée.
Celle-ci appelle fake_callback, qui échoue.
Puis pnlfs_mount appell pnlfs_unmount.
Puis pnlfs_mount se termine

pnlfs_mount
-> fake_callback
-> pnlfs_unmount

## 5. Superbloc

Maintenant que notre FS est enregistré et permet de monter une partition, il faut lire les métadonnées de la partition afin de correctement manipuler les données.

Les fonctions représentées par le superblock sont représentés dans le fichier super.c. Voici leurs en-têtes :

    struct pnlfs_sb_info *sbi;

    /* That function undo all the change made by fill_super function */
    static void pnlfs_put_super(struct super_block *sb)

    /* Inode constructor */
    static struct inode *pnlfs_alloc_inode(struct super_block *sb)

    /* Inode destructor */
    static void pnlfs_destroy_inode(struct inode *i)

    /* List of operations */
    struct super_operations pnlfs_op = {
      .put_super = pnlfs_put_super,
      .alloc_inode = pnlfs_alloc_inode,
      .destroy_inode = pnlfs_destroy_inode,
    };

    /* CallBack called during mount, fill the super block */
    static int pnlfs_fill_super(struct super_block *sb, void *data, int flags)

    /* Function called for mounting, set in file_system_type */
    static struct dentry *pnlfs_mount
    (struct file_system_type *fs, int flags, const char *dev, void *data)

    /* Function called for unmounting, set in file_system_type */
    static void pnlfs_kill_super(struct super_block *sb)

    static struct file_system_type pnlfs_fs_type = {
      .owner    = THIS_MODULE,
      .name   = "pnlfs",
      .mount    = pnlfs_mount,
      .kill_sb  = pnlfs_kill_super,
      .fs_flags = FS_REQUIRES_DEV,
    };

## 6. Recherche d'inode

L’accès au dossier où l’image est montée est toujours impossible car le VFS n’est pas capable de trouver l’inode correspondante au nom du fichier dans le cache des dentry. Pour résoudre ce problème, implémenter la fonction lookup() de la struct inode_operations.
Voici la fonction lookup :

    /* Search for inodes */
    static struct dentry *pnlfs_lookup
    (struct inode *dir, struct dentry *dentry, unsigned int flags)
    {
      struct pnlfs_inode_info *inode_info;
      struct buffer_head *bh;
      struct pnlfs_dir_block *dir_block;
      struct pnlfs_file *files;
      struct inode *inode;
      int i;

      pr_info("%s Start\n",  __func__);

      /* Read block */
      inode_info = container_of(dir, struct pnlfs_inode_info, vfs_inode);
      bh = sb_bread(dir->i_sb, inode_info->index_block);
      if (bh == NULL)
        return ERR_PTR(-EIO);

      /* Get files contained in that inode */
      dir_block = (struct pnlfs_dir_block *) bh->b_data;
      files = dir_block->files;
      brelse(bh);
      inode = NULL;
      /* Search in the block for the looked inode */
      for (i = 0; i < PNLFS_MAX_DIR_ENTRIES; i++) {
        if(!strcmp(files[i].filename, dentry->d_name.name)){
          inode = pnlfs_iget(dir->i_sb, files[i].inode);
          if (IS_ERR(inode))
            return (ERR_PTR(PTR_ERR(inode)));
          break;
        }
      }

      /* Add dentry to hash queues */
      d_add(dentry, inode);

      pr_info("%s End\n",  __func__);
      return NULL;
    }

Il est maintenant possible d’accéder au dossier racine de votre image montée !

    mount -t pnlfs -o loop /tmp/disk.img /root/mnt/
    cd /root/mnt
    pwd
    > /root/mnt

## 7. Opération sur les répertoires

Afin de pouvoir connaı̂tre la contenu d’un répertoire, implémenter la fonction iterate shared() de votre struct file_operations. Voici la fonction pnlfs_iterate_shared :

    /* Function called iteratively, with the same context */
    static int pnlfs_iterate_shared(struct file *file, struct dir_context *ctx)
    {
      struct inode * inode;
      struct pnlfs_inode_info *inode_info;
      struct buffer_head *bh;
      struct pnlfs_dir_block *dir_block;
      struct pnlfs_file *f;
      int i;
      bool err = false;
      pr_info("%s Start\n", __func__);

      /* Check if the name length is not too long */
      if (file->f_path.dentry->d_name.len > PNLFS_FILENAME_LEN)
        return -ENAMETOOLONG;

      /* Add the file . and the file .. */
      if(!dir_emit_dots(file, ctx))
        return -EACCES;

      /* Get the inode of current file */
      inode = file_inode(file);
      inode_info = container_of(inode, struct pnlfs_inode_info, vfs_inode);
      /* Check if the cursor hasn't passed all the files */
      if (ctx->pos >= (le32_to_cpu(inode_info->nr_entries) + 2)) {
        pr_info("%s End\n", __func__);
        return 0;
      }

      /* Read the block of the inode */
      bh = sb_bread(inode->i_sb, inode_info->index_block);
      if (bh == NULL)
        return -EIO;

      /* Get the list of files */
      dir_block = (struct pnlfs_dir_block *) bh->b_data;
      brelse(bh);
      f = dir_block->files;

      /* For each files, add it to the list via dit_emit function */
      for (i = 0; i < PNLFS_MAX_DIR_ENTRIES; i++) {
        if (f[i].inode == 0)
          continue;
        if ((err = !dir_emit(ctx, f[i].filename, strlen(f[i].filename),
          f[i].inode, DT_UNKNOWN))) {
          break;
        }
      }

      /* Increase the offset */
      ctx->pos += inode_info->nr_entries;

      pr_info("%s Next\n", __func__);
      if(err)
        return -EACCES;
      return 0;
    }

Et la structure file_operations associée :

    struct file_operations i_fop = {
      .iterate_shared = pnlfs_iterate_shared,
    };

Vous devriez désormais pouvoir lister le contenu d’un répertoire avec la commande ls.

    mount -t pnlfs -o loop /tmp/disk.img /root/mnt/
    cd /root/mnt
    ls
    > foo

## 8. Opérations sur les inodes

La plupart des fonctionnalités d’un FS sont effectuées au niveau de l’inode, via la structure struct inode_operations.

Voici les en-têtes des fonctions d'inodes :

    #include "pnlfs.h"

    /* That function ask a struct inode to the VFS */
    struct inode *pnlfs_iget(struct super_block *sb, unsigned long ino)

    /* Free the inode bit at index ino in the bitmap */
    int pnlfs_free_inode(struct super_block *sb, unsigned long ino)

    /* Register a new inode in the bitmap and return its index */
    unsigned long pnlfs_reserv_new_inode(struct super_block *sb)

    /* Search for inode with macthing name in directory */
    unsigned long pnlfs_find_inode(struct inode *dir, struct dentry *dentry)

    /* Register a new block in the bitmap and return its index */
    int pnlfs_reserv_new_block(struct super_block *sb)

    /* Free the inode bit at index ino in the bitmap */
    int pnlfs_free_block(struct super_block *sb, int bno)

    /* Change the block according to the dentry */
    int pnlfs_add_entry
    (struct inode * dir, struct dentry * dentry, unsigned long ino)

    /* Remove the dentry from the block */
    int pnlfs_delete_entry(struct inode *dir, unsigned long ino)

    /*****************************
    ****inode_operations
    *****************************/

    /* Search for inodes */
    static struct dentry *pnlfs_lookup
    (struct inode *dir, struct dentry *dentry, unsigned int flags)

    /* Set new inode and save change to the block */
    static int pnlfs_create
    (struct inode *dir, struct dentry *dentry, umode_t mode, bool unused)

    static int pnlfs_unlink(struct inode *dir, struct dentry *dentry)

    /* Create a directory */
    static int pnlfs_mkdir(struct inode *dir, struct dentry *dentry, umode_t mode)

    /* Create a directory */
    static int pnlfs_rmdir(struct inode *dir, struct dentry *dentry)

    /* Change the name of a dentry */
    int pnlfs_rename
    (struct inode *old_dir,
     struct dentry *old_dentry,
     struct inode *new_dir,
     struct dentry *new_dentry,
     unsigned int flags)


    struct inode_operations i_op = {
      .lookup = pnlfs_lookup,
      .create = pnlfs_create,
      .unlink = pnlfs_unlink,
      .mkdir = pnlfs_mkdir,
      .rmdir = pnlfs_rmdir,
      .rename = pnlfs_rename
    };

Vous devriez désormais être capable de créer, renommer et supprimer des fichiers et des répertoires dans votre FS.

    mount -t pnlfs -o loop /tmp/disk.img /root/mnt/
    cd /root/mnt
    ls
    > foo
    touch merguez
    > foo merguez

## 9. Opérations sur les fichiers

Les opérations sur les fichiers sont implémentées dans la struct file_operations de l’inode. Voici les deux fonctions ajoutées pour pouvoir réaliser des opérations sur les fichiers :

    /* Read block */
    static ssize_t pnlfs_read
    (struct file * filp, char __user * buf, size_t count, loff_t * pos)

    /* Write block */
    ssize_t pnlfs_write(struct file *filp, const char __user *buf, size_t count,
            loff_t *pos)

    struct file_operations i_fop = {
      .iterate_shared = pnlfs_iterate_shared,
      .read = pnlfs_read,
      .write = pnlfs_write
    };

Vous devriez désormais pouvoir lire et écrire des fichiers dans votre FS.

    mount -t pnlfs -o loop /tmp/disk.img /root/mnt/
    cd /root/mnt
    ls
    > foo
    echo saucisse > merguez
    cat merguez
    > saucisse

## 10. Persistance

Pour rendre notre FS persistant, implémenter les fonctions sync fs() et write inode() de la struct super_operations.

    int pnlfs_sync_fs(struct super_block *sb, int wait)

    static
    int pnlfs_write_inode(struct inode *inode, struct writeback_control *wbc)

    /* List of operations */
    struct super_operations pnlfs_op = {
      .write_inode = pnlfs_write_inode,
      .sync_fs = pnlfs_sync_fs,
    };

Notre FS est maintenant persistant !

    mount -t pnlfs -o loop /tmp/disk.img /root/mnt/
    cd /root/mnt
    ls
    > foo
    echo saucisse > merguez
    cat merguez
    > saucisse
    # Démontage
    unmount /root/mnt
    # Second montage
    mount -t pnlfs -o loop /tmp/disk.img /root/mnt/
    cd /root/mnt
    ls
    > foo merguez
    cat merguez
    > saucisse

